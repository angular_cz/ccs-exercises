DI - 01

1. vyzkoušejte funkčnost kalkulačky a podívejte se na funkcionalitu v controlleru
  - poté ověřte, že projdou testy, které funkčnost testují
  - spusťte v terminálu příkaz:

    npm run di-01-karma

2. Přesuňte defaultní objekt do value
  - vytvořte value defaultProduct pomocí .value,
  - přesuňte do ní nastavení productu z controlleru.

    ... zdrojový kód TODO 2.1 - vytvořte value defaultProduct -->
                                                                                                           .value('defaultProduct', {
                                                                                                              pageSize: 'A5',
                                                                                                              numberOfPages: 50
                                                                                                            })
  - použít tuto value v controlleru CalculatorCtrl
  - nezapomeňte value do controlleru injectnout
    ... zdrojový kód TODO 2.2 - použití value v controlleru -->
                                                                                                          .controller('CalculatorCtrl', function (defaultProduct) {
                                                                                                              this.product = defaultProduct;
3. Přesuňte logiku z controlleru do factory
  - vytvořte factory calculator, která vrací object literal
  - přesuňte sem metodu getPrice a přidejte parametr product
    ... zdrojový kód TODO 3.1 - vytvořte factory calculator -->
                                                                                                          .factory('calculator', function () {
                                                                                                            return {
                                                                                                              getPrice: function (product) {
                                                                                                                ...

                                                                                                                return price;
                                                                                                              }
                                                                                                            };
                                                                                                          })
  - injektněte factory do controlleru a zavolejte ji, z funkce getPrice s parametrem this.product.
    ... zdrojový kód TODO 3.2 - použijte factory calculator -->
                                                                                                          .controller('CalculatorCtrl', function (defaultProduct, calculator) {
                                                                                                            ...

                                                                                                            this.getPrice = function () {
                                                                                                              return calculator.getPrice(this.product);
                                                                                                            };
  - ověřte, že projdou testy

    npm run di-01-karma

4. Oddělení logování z service calculator
  - vytvořte service pojmenovanou logger
  - vytvořit metodu log(message), která loguje do console
  - nezapomeňte, že service přijímá konstrukční funkci
    ... zdrojový kód TODO 4.1 - vytvořte service logger -->
                                                                                                          .service('logger', function(){
                                                                                                              this.log = function(message){
                                                                                                                console.log(message);
                                                                                                              };
                                                                                                            })
  - injektněte logger do service calculator a zaměňte volání console.log za logger.log
    ... zdrojový kód TODO 4.2 - vytvořte service logger -->
                                                                                                          .factory('calculator', function (logger) {
                                                                                                             ...
                                                                                                             logger.log('Price : ' ...
