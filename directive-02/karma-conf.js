module.exports = function(config) {
  config.set({
    basePath: '../',
    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'directive-02/*.js'
    ],
    frameworks: ['jasmine'],
    browsers: ['Chrome'], // 'Firefox', 'PhantomJS'
    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-nested-reporter'
    ],
    autoWatch: true,
    singleRun: false,
    reporters: ['nested']
  });
};