angular.module('authApp')

  .factory('Orders', function (REST_URI, $resource) {
    return $resource(REST_URI + '/auth-02/orders/:id', {"id": "@id"});
  })

  .factory('authService', function ($http, REST_URI, $rootScope) {

    var checkLoginResponse = function (auth, response) {
      if (response.status == 200) {
        auth.user = response.data;
        $http.defaults.headers.common['X-Auth-Token'] = response.data.token;
        $rootScope.$broadcast("login:changedState");
      }

      return auth;
    };

    var auth = {
      user: null,

      login: function (name, pass) {
        return $http.post(REST_URI + "/login", {name: name, password: pass})
          .then(function (response) {
            return checkLoginResponse(auth, response);
          });
      },

      logout: function () {
        $http.post(REST_URI + "/logout");
        delete this.user;
        delete $http.defaults.headers.common['X-Auth-Token'];

        $rootScope.$broadcast("login:loggedOut");
        $rootScope.$broadcast("login:changedState")
      },

      hasRole: function (role) {
        if (!this.user) {
          return false;
        }

        if (!this.user.roles) {
          return false;
        }

        return this.user.roles.indexOf(role) !== -1;
      },

      isAuthenticated: function () {
        return this.user;
      }
    };

    return auth;

  })

  .service('loginModal', function ($modal) {

    this.modalInstance = null;

    this.createModalInstance = function (template) {
      this.modalInstance = $modal.open({
        templateUrl: template || 'authModal.html',
        controller: 'AuthCtrl',
        controllerAs: 'auth'
      });

      this.modalInstance.result.then(function (data) {
        delete this.modalInstance;

        return data;
      }.bind(this), function () {
        delete this.modalInstance;
      }.bind(this));
    };

    this.prepareLoginModal = function () {
      if (!this.modalInstance) {
        this.createModalInstance("authModal.html");
      }

      return this.modalInstance.result;
    };

    this.prepareRejectModal = function () {
      if (!this.modalInstance) {
        this.createModalInstance("rejectModal.html");
      }

      return this.modalInstance.result;
    };

  });
  