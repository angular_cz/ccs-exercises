Controller-01

1. Přidání vazby na controller do šabloně
 - podívejte se na definici controlleru v app.js
 - v index.html si všimněte v html tagu definice ng-app
 - přidejte na element body vazbu na controller pomocí directivy ng-controller
   ... zdrojový kód TODO 1 - přidejte vazbu na controller
                                                                                                          <body ng-controller="UserController" class="container">
2. Inicializujte objekt user ve $scope v controlleru v app.js
 - do $scope.user přiřaťte objekt níže

      {
        name: 'Petr',
        surname: 'Novák',
        yearOfBirth: 1982
      };

3. Napojte formulářové prvky na scope
 - připojte jednotlivé attributy objektu user na formulářové prvky input pomocí directivy ng-model
   ... zdrojový kód TODO 3 - přidejte vazbu na objekt user
                                                                                                          <input id="userName"    ng-model="user.name"        class="form-control">
                                                                                                          <input id="userSurname" ng-model="user.surname"     class="form-control">
                                                                                                          <input id="yearOfBirth" ng-model="user.yearOfBirth" class="form-control" type="number">
 - pravý panel s výpisem již obsahuje výrazy
 - vyzkoušejte tedy, že funguje dvoucestný databinding

4. V controlleru přiřaďte již definovanou funkci getAge na $scope
  ... zdrojový kód TODO 4 - přiřaďte funkci getage do $scope
                                                                                                          $scope.getAge = function () {
                                                                                                            var now = new Date();
                                                                                                            return now.getFullYear() - $scope.user.yearOfBirth;
                                                                                                          };
 - metoda je v šabloně už použita
 - vyzkoušejte tedy, jak se nyní aplikace chová
